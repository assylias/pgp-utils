/*
 * Copyright 2013 Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.encryption;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import static org.testng.Assert.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author Yann Le Tallec
 */
public class PGPCryptographyTest {

    private static final File publicKey = Paths.get("./src/test/resources/public_key.txt").toFile();
    private static final File privateKey = Paths.get("./src/test/resources/private_key.txt").toFile();
    private static final char[] PASSWORD = "password".toCharArray();
    private static final Path original = Paths.get("./src/test/resources/test_file.txt");
    private static final Path encrypted = Paths.get("./src/test/resources/test_file_encrypted.pgp");
    private static final Path decrypted = Paths.get("./src/test/resources/test_file_decrypted.txt");

    @BeforeClass
    public void beforeClass() throws Exception {
        if (Files.exists(encrypted)) Files.delete(encrypted);
        if (Files.exists(decrypted)) Files.delete(decrypted);
    }

    @Test
    public void signAndEncrypt() throws Exception {
        PGPCryptography crypto = new PGPCryptography(publicKey, privateKey);
        crypto.signAndEncryptSinglePass(original.toFile(), encrypted.toFile(), PASSWORD);
        File result = encrypted.toFile();
        assertTrue(result.exists());
        assertTrue(result.length() > 0);
    }

    @Test(dependsOnMethods = "signAndEncrypt")
    public void decrypt() throws Exception {
        PGPCryptography crypto = new PGPCryptography(publicKey, privateKey);
        crypto.decryptAndVerify(encrypted.toFile(), decrypted.toFile(), PASSWORD);
        List<String> originalLines = Files.readAllLines(original, Charset.forName("UTF-8"));
        List<String> decryptedLines = Files.readAllLines(decrypted, Charset.forName("UTF-8"));
        assertEquals(decryptedLines, originalLines);
    }

}